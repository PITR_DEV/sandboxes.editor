# SandBoxes.Editor

A map bundle editor for SandBoxes Rewritten


# Getting Started

## Get Github  Desktop
This is very important!
You can't just download this repo via the download button above, because it won't work.
We are using git lfs for big files, and because of this you need to download it through a git client, preferably github desktop.

Here is the link: [https://desktop.github.com/](https://desktop.github.com/)
Once it's installed, go to File > Clone Repository and paste the URL of this repository in.

Updates are pushed semi regularly, so find out how to keep the repo up to date (Fetch Origin, or Repository > Pull).

## Get Unity

This project is using Unity 2019.2 - grab it here: [https://unity3d.com/get-unity/update](https://unity3d.com/get-unity/update)

## Open Unity

Open unity and launch this project. Navigate to Assets/Maps/Examples and open the `Basic Demo` map

You should see something like this:

**At this point in time, SandBoxes Rewritten isn't out for testing yet, but you can already begin creating maps.**


## Exporting

To export your map, use the SandBoxes drop-down menu and select export.

## Help and Community

Discord: [https://discord.gg/sandboxes](https://discord.gg/sandboxes)

## Thanks
to Garry Newman for the original Tub.Editor